import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_archi/model/movie_model.dart';
import 'package:test_archi/view/movie_detail_view.dart';
import 'package:test_archi/view_model/movie_view_model.dart';


class MovieListWidget extends StatelessWidget {
  const MovieListWidget();

  @override
  Widget build(BuildContext context) {
    return  Consumer<MovieViewModel>(
      builder: (_, movieVM, __) => Expanded(
        child: ListView.builder(
          itemCount: movieVM.movies.length,
          itemBuilder: (context, index) {
            final MovieModel movie = movieVM.movies[index];

            return ListTile(
              contentPadding: const EdgeInsets.all(10),
              leading: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage('https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-with-glasses.jpg'),
                    ),
                    borderRadius: BorderRadius.circular(6)
                ),
                width: 50,
                height: 100,
              ),
              title: Text(movie.title ?? ''),
              onTap: () async {
                if(movie.id != null){
                  Provider.of<MovieViewModel>(context, listen: false).setSelectedMovie(movie.id.toString());
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> const MovieDetailView(),),);
                }
              },
            );
          },
        ),
      ),
    );
  }
}