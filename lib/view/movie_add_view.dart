import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_archi/model/movie_model.dart';
import 'package:test_archi/view/home_view.dart';
import 'package:test_archi/view_model/movie_view_model.dart';


class MovieAddView extends StatefulWidget {
  MovieAddView({Key? key}) : super(key: key);

  @override
  _MovieAddViewState createState() => _MovieAddViewState();
}

class _MovieAddViewState extends State<MovieAddView> {
  MovieModel movie = new MovieModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Movie add'),
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(
              onChanged: (value){
                movie.title = value;
              },
            ),
            TextFormField(
              onChanged: (value){
                print(value);
                movie.type = value;
              },
            ),
            TextFormField(
              onChanged: (value){
                movie.year = value;
              },
            ),
            TextFormField(
              onChanged: (value){
                movie.poster = value;
              },
            ),
            TextButton(
                onPressed: () async{
                  bool isAdded = await Provider.of<MovieViewModel>(context, listen: false).addMovie(movie);
                  if(isAdded){
                    Provider.of<MovieViewModel>(context, listen: false).getAllMovies();
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context)=> HomeView(),
                      ),
                    );
                  } else {
                    print('oups');
                  }
                },
                child: const Text('Add'),
            ),
          ],
        ),
    );
  }
}
