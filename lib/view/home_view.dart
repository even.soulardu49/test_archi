import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_archi/view/movie_add_view.dart';
import 'package:test_archi/view_model/movie_view_model.dart';
import 'package:test_archi/widget/movie_list_widget.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
            title: const Text("Movies"),
          leading: IconButton(
            onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=> MovieAddView(),),);
            },
            icon: const Icon(Icons.add),
          ),
        ),
        body: Container(
            padding: const EdgeInsets.all(10),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Column(children: <Widget>[
              Container(
                padding: const EdgeInsets.only(left: 10),
                decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: TextFormField(
                  onFieldSubmitted: (value) async {
                    Provider.of<MovieViewModel>(context, listen: false).getAllMovies(keyword: value);
                  },
                  style: const TextStyle(color: Colors.white),
                  decoration: const InputDecoration(
                      hintText: "Search",
                      hintStyle: TextStyle(color: Colors.white),
                      border: InputBorder.none
                  ),

                ),
              ),
              MovieListWidget(),
            ])
        )

    );
  }
}