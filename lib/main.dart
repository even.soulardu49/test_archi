import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_archi/view/error_view.dart';
import 'package:test_archi/view/home_view.dart';
import 'package:test_archi/view/loading_view.dart';
import 'package:test_archi/view_model/movie_view_model.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => MovieViewModel()),
      ],
      child: const App(),
    ),
  );
}

class App extends StatefulWidget {

  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Initialize FlutterFire:
      future: _initialization,
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          print(snapshot.error);
          return  MaterialApp(
            home: ErrorView(),
          );
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          return MaterialApp(
            home: HomeView(),
          );
        }

        // Otherwise, show something whilst waiting for initialization to complete
        return  MaterialApp(
          home: LoadingView(),
        );
      },
    );
  }
}
