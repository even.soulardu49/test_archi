class MovieModel {

  String? title;
  String? poster;
  String? type;
  String? year;
  String? id;

  MovieModel({this.title, this.poster, this.type, this.year, this.id});

  factory MovieModel.fromJson(Map<String, dynamic> json) {
    return MovieModel(
        title: json["title"],
        poster: json["poster"],
        type: json["type"],
        year: json["year"],
        id: json["id"],
    );
  }

  Map<String, dynamic> toJSON() => {
    'poster': this.poster,
    'title': this.title,
    'type': this.type,
    'year': this.year,
  };

}