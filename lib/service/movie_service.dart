import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:test_archi/model/movie_model.dart';

import 'package:http/http.dart' as http;

abstract class MovieService {

  static CollectionReference _movieRepository = FirebaseFirestore.instance.collection('movie');

  static Future<List<MovieModel>> getAllMovies(String? keyword) async {
    List<MovieModel> movies = [];
    try{
      final snapshot =
      keyword != null ?
      await _movieRepository
          .where('title', isGreaterThanOrEqualTo: keyword)
          .where('title', isLessThan: keyword + 'z')
          .get() :
      await _movieRepository.get();
      for(QueryDocumentSnapshot movie in snapshot.docs){
        Map<String, dynamic> data = movie.data() as Map<String, dynamic>;
        data['id'] = movie.id;
        movies.add(MovieModel.fromJson(data));
      }
    }
    catch(e) {
      print(e);
    }
    return movies;
  }

  static Future<MovieModel> getMovie(String movieId) async {
    final snapshot = await _movieRepository.doc(movieId).get();
    if(snapshot.exists){
      Map<String, dynamic> data = snapshot.data() as Map<String, dynamic>;
      data['id'] = snapshot.id;
      return MovieModel.fromJson(data);
    }else{
      throw new Exception('this movie doesn\'t exist');
    }
  }

  static Future<bool> addMovie(MovieModel movie) async {
    try{
      await _movieRepository.add(movie.toJSON());
      return true;
    }catch(e){
      print(e);
      return false;
    }
  }
}