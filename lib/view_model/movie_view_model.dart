import 'package:flutter/material.dart';
import 'package:test_archi/model/movie_model.dart';
import 'package:test_archi/service/movie_service.dart';

class MovieViewModel extends ChangeNotifier {
  List<MovieModel> movies = [];
  MovieModel? selectedMovie;

  MovieViewModel() {
    getAllMovies();
  }

  Future<void> getAllMovies({String? keyword}) async {
    this.movies.clear();
    this.movies = await MovieService.getAllMovies(keyword);
    notifyListeners();
  }

  Future<void> setSelectedMovie(String movieId) async {
    this.selectedMovie = await MovieService.getMovie(movieId);
    notifyListeners();
  }

  Future<bool> addMovie(MovieModel movie) async {
    return await MovieService.addMovie(movie);
  }

}