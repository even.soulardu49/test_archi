import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_archi/view_model/movie_view_model.dart';

class MovieDetailView extends StatelessWidget {
  const MovieDetailView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Movie detail'),
      ),
      body: Consumer<MovieViewModel>(
        builder: (_, movieVM, __) => Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('title: ${movieVM.selectedMovie?.title}'),
              Text('type: ${movieVM.selectedMovie?.type}'),
              Text('poster: ${movieVM.selectedMovie?.poster}'),
              Text('year: ${movieVM.selectedMovie?.year}'),
              Text('id: ${movieVM.selectedMovie?.id}'),
            ],
          ),
        ),
      ),
    );
  }
}
